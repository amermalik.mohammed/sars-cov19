# SARS-Cov19 Project

Coronavirus disease 2019 (COVID-19) is a serious and very actual concern nowadays. Data is disaggregated by country (and sometimes subregion). This project aims to postprocess public accesible data from Czech Republic and to deliver some insights into the topic. Mainly, the project should show how the relevant numbers are developing for selected hospitals (eg. number of vaccination already performed in certain hospital).

Coronavirus disease (COVID-19) is caused by the [Severe acute respiratory syndrome Coronavirus 2 (SARS-CoV-2)][sars2] and has had a worldwide effect. On March 11 2020, the World Health Organization (WHO) declared it a pandemic, pointing to the over 118,000 cases of the Coronavirus illness in over 110 countries and territories around the world at the time.

[covid]: https://en.wikipedia.org/wiki/Coronavirus_disease_2019
[sars2]: https://en.wikipedia.org/wiki/Severe_acute_respiratory_syndrome_coronavirus_2

The project is currently under development. It should contain the following data set in the near future:

- confirmed tested cases of Coronavirus infection
- the number of people who have reportedly died while sick with Coronavirus
- the number of people who have reportedly recovered from it
- the number of people who were vaccinated
- the number and type of vaccination used
- and many more...


## Authorship

The project was created on the 26th of March 2021 by **Petr Melichar**. You are welcome to contact the author of project with any concerns. Any comment or suggestion are very appreciated. 

## Preparation

This repository uses several python libraries that are needed for the python script to work correctly. The most crucial ones are [Pandas](https://pandas.pydata.org), [Numpy](https://numpy.org/) and [Matplotlib](https://matplotlib.org/). The full list of all required packages may be found in the file [requirements.txt](requirements.txt).

This project is currently implemented as a (single) Jupyter notebook. The easiest way to get started is to use appropriate services to open the project, eg. Binder or Jupyter GWDG service.

You first need to install the dependencies:

```bash
pip install -r requirements.txt
```

Then just open the jupyter notebook, please open the file **main.ipynb**, which is located in the folder *notebooks*. Although it is in principle possible to open the notebook from the terminal, this is not recommended way to procceed. Instead, you can open the neccesary file directly by clicking on the right file in the *file browser toolbar* on the left. 

It is possible to run execute the file with python3 by using the following command in commandline. However, in the current version of the project, no output will be generated at all. The project was implemented to work as a jupyter notebook, where the text in the surrounding fields is supplementing the plots. Thus, the following command should not be used. 


```bash
python notebooks/main.ipynb
```

[![Python 3.8](https://img.shields.io/badge/python-3.8-blue.svg)](https://www.python.org/downloads/release/python-380/)
![Master branch](https://gitlab.gwdg.de/petr.melichar/sars-cov19/-/tree/master)



## License

This dataset is licensed under the Open Data Commons [Public Domain and Dedication License][pddl].

[pddl]: https://www.opendatacommons.org/licenses/pddl/1-0/

The data comes from a variety public sources, mainly from [COVID-19 v ČR: Otevřené datové sady a sady ke stažení](https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19). The original data were colated by Czech Statistical Office and the Ministry of Health and made accesible for everyone as a corresponding csv data file. I have used that data and processed it further. Given the public sources and factual nature I believe that there the data is public domain and are therefore releasing the results under the Public Domain Dedication and License. I am also, of course, explicitly licensing any contribution of mine under that license.


